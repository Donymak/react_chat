import {get} from '../../helpers/http.helper'

const User = (() => {
    const mock = (): Promise<any> => get('mock')
    return {
        mock,
    }
})()

export {User}