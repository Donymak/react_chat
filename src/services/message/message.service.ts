import {get} from '../../helpers/http.helper'

const Message = (() => {
    const mock = (): Promise<any> => get('mock')
    return {
        mock,
    }
})()

export {Message}