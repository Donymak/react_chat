import auth from './auth/auth.service'
import * as message from './message/message.service'
import * as user from './user/user.service'
import storage from './storage/storage.service'

export {
    auth,
    message,
    user,
    storage,
}