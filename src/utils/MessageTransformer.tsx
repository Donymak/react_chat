import {IMessage} from "../interfaces/common/message";

const MessageTransformer = (() => {

    const _transform = (message: IMessage): IMessage => {
        return {
            id: message.id,
            userId: message.userId,
            avatar: message.avatar,
            user: message.user,
            text: message.text,
            createdAt: new Date(message.createdAt),
            editedAt: message.editedAt ? new Date(message.editedAt) : undefined,
            liked: false
        }
    }

    const _transformAll = (messages: IMessage[]): IMessage[] => {
        return messages.map(message => _transform(message))
    }

    return {
        transform: _transform,
        transformAll: _transformAll
    }
})()

export default MessageTransformer;