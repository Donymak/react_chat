import axios from 'axios'
import { ENV } from '../common/enums/app/env.enum'

const axiosApi = axios.create({
    baseURL: ENV.API_PATH,
})

const get = (url: string, config = {}): Promise<any> => axiosApi.get(url, { ...config })

const post = (url: string, data: any, config = {}): Promise<any> => axiosApi.post(url, { ...data }, { ...config })

const put = (url: string, data: Record<string, unknown>, config = {}): Promise<any> => axiosApi.put(url, { ...data }, { ...config })

const patch = (url: string, data: Record<string, unknown>, config = {}): Promise<any> => axiosApi.patch(url, { ...data }, { ...config })

const del = (url: string, config = {}): Promise<any> => axiosApi.delete(url, { ...config })

export {
    get, post, put, patch, del,
}