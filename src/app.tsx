import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import React from 'react'
import Login from './pages/login'
import Chat from './pages/chat'
import EditUsers from './pages/edit_users'
import EditUser from './pages/edit_user'
import Users from './pages/users'

const App: React.FC = () => (
    <>
        <Router>
            <Routes>
                <Route path="/login" element={<Login/>}/>
                <Route path="/" element={<Chat/>}/>
                <Route path="/users" element={<Users/>}/>
                <Route path="/users/edit" element={<EditUsers/>}/>
                <Route path="/users/edit/:id" element={<EditUser/>}/>
            </Routes>
        </Router>
    </>
)

export default App