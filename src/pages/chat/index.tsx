import React, {useEffect, useMemo, useState} from "react";
import {v4 as uuidv4} from "uuid";
import {ApiService} from "../../services/api/index.ts";
import Header from "../../components/header";
import MessageList from "../../components/message_list";
import MessageInput from "../../components/message_input";
import {Preloader} from "../../components/preloader";
import {getUser} from "../../mock_auth/auth";
import {IMessage} from "../../interfaces/common/message";
import {IUser} from "../../interfaces/common/user";

import MessageTransformer from "../../utils/MessageTransformer";
import {getFormattedDateForLastMessage} from "../../utils/DateFormater";
import {countUniqueParticipants, getLastMessageDate} from "./utils";
import {prepareMessagesForRender} from "./helpers";


const Chat: React.FC = () => {
    const apiService = useMemo(
        () => new ApiService(
            'https://edikdolynskyi.github.io/react_sources/messages.json',
            ), []);

    const [messages, setMessages] = useState<IMessage[]>([])
    const [loading, setLoading] = useState<boolean>(true)
    const [isEditing, setIsEditing] = useState<boolean>(false)
    const [editingMessage, setEditingMessage] = useState<string>('')
    const [editingMessageId, setEditingMessageId] = useState<string>('')

    useEffect(() => {
        try {
            apiService.fetchMessages()
                .then(messages => MessageTransformer.transformAll(messages))
                .then(transformedMessages => {
                    setMessages(transformedMessages.sort((message1, message2) => message1.createdAt > message2.createdAt ? 1 : -1))
                    setLoading(false)
                })
        } catch (error) {
            console.log(error)
        }
    }, [apiService])

    const addMessage = (message: string): void => {
        const messagesCopy: IMessage[] = [...messages];

        const user: IUser = getUser();

        messagesCopy.push({
            id: uuidv4(),
            userId: user.id,
            avatar: user.avatar,
            user: user.username,
            text: message,
            createdAt: new Date(),
            liked: false
        });

        setMessages(messagesCopy)
    }

    const toggleLike = (id: string): void => {
        const messagesCopy: IMessage[] = [...messages];

        const index = messagesCopy.findIndex(message => message.id === id);
        if (messagesCopy[index]) {
            messagesCopy[index].liked = !messagesCopy[index]?.liked;
            setMessages(messagesCopy)
        }
    }

    const deleteMessage = (id: string): void => {
        const messagesCopy: IMessage[] = [...messages];

        const index = messagesCopy.findIndex(message => message.id === id);
        if (messagesCopy[index]) {
            messagesCopy.splice(index, 1)
            setMessages(messagesCopy)
        }
    }

    const editMessage = (text: string): void => {
        const messagesCopy: IMessage[] = [...messages];

        const index = messagesCopy.findIndex(message => message.id === editingMessageId);
        if (messagesCopy[index]) {
            messagesCopy[index].text = text;
            messagesCopy[index].editedAt = new Date();

            setMessages(messagesCopy)
            setEditingMessageId('')
            setEditingMessage('')
            setIsEditing(false)
        }
    }

    const invokeEditionMessage = (id: string, text: string): void => {
        setEditingMessageId(id)
        setEditingMessage(text)
        setIsEditing(true)
    }

    if (loading) {
        return <Preloader/>
    }

    return (
        <div className="chat-container">
            <Header
                chatName={"World Chat"}
                participantsCount={countUniqueParticipants(messages)}
                messagesCount={messages.length}
                lastMessageDate={getFormattedDateForLastMessage(getLastMessageDate(messages))}
            />
            <MessageList>
                {prepareMessagesForRender(
                    messages,
                    toggleLike,
                    invokeEditionMessage,
                    deleteMessage
                )}
            </MessageList>
            <MessageInput onAddMessage={addMessage}
                          onEditMessage={editMessage}
                          text={editingMessage}
                          isEditing={isEditing}/>

        </div>
    )
}

export default Chat;