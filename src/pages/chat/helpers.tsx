import React from 'react'
import { v4 as uuidv4 } from 'uuid'
import { IMessage } from '../../interfaces/common/message'
import {
    getFormattedDateForDivider,
    getFormattedTimeForMessage,
} from '../../utils/DateFormater'
import { getUser } from '../../mock_auth/auth'
import { OwnMessage } from '../../components/message/ownMessage'
import { Message } from '../../components/message/message'

const convertMessageArrayToMap = (
    messages: IMessage[],
): Map<string, IMessage[]> => {
    const messagesByDay: Map<string, IMessage[]> = new Map<string, IMessage[]>()

    messages.forEach((message) => {
        const day = getFormattedDateForDivider(message.createdAt)

        if (messagesByDay.has(day)) {
            const messagesBySpecificDay = messagesByDay.get(day)
            if (messagesBySpecificDay) {
                messagesBySpecificDay.push(message)
                messagesByDay.set(day, messagesBySpecificDay)
                return
            }
        }
        messagesByDay.set(day, Array.of(message))
    })

    return messagesByDay
}

const convertMessagesByDayMapToJsx = (
    map: Map<string, IMessage[]>,
    onLikeHandler: (id: string) => void,
    onEditHandler: (id: string, text: string) => void,
    onDeleteHandler: (id: string) => void,
): JSX.Element[] => {
    const renderOutput: JSX.Element[] = []

    map.forEach((messages, day) => {
        renderOutput.push(
            <div key={uuidv4()} className="message-divider">
                {day}
            </div>,
        )
        messages.forEach((message) => {
            if (message.userId === getUser().id) {
                renderOutput.push(
                    <OwnMessage
                        key={message.id}
                        text={message.text}
                        avatar={message.avatar}
                        user={message.user}
                        editedAt={message.editedAt}
                        createdAt={getFormattedTimeForMessage(message.createdAt)}
                        onDelete={() => onDeleteHandler(message.id)}
                        onEdit={() => onEditHandler(message.id, message.text)}
                    />,
                )
            } else {
                renderOutput.push(
                    <Message
                        key={message.id}
                        id={message.id}
                        userId={message.userId}
                        avatar={message.avatar}
                        user={message.user}
                        text={message.text}
                        createdAt={getFormattedTimeForMessage(message.createdAt)}
                        liked={message.liked}
                        onLike={() => onLikeHandler(message.id)}
                    />,
                )
            }
        })
    })

    return renderOutput
}

export const prepareMessagesForRender = (
    messages: IMessage[],
    onLikeHandler: (id: string) => void,
    onEditHandler: (id: string, text: string) => void,
    onDeleteHandler: (id: string) => void,
): JSX.Element[] => {
    const messagesByDayMap = convertMessageArrayToMap(messages)

    return convertMessagesByDayMapToJsx(
        messagesByDayMap,
        onLikeHandler,
        onEditHandler,
        onDeleteHandler,
    )
}