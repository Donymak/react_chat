import {IMessage} from "../../interfaces/common/message";

export const countUniqueParticipants = (messages: IMessage[]): number => {
    const participants: string[] = []

    messages.forEach(message => {
        if (!participants.includes(message['userId'])) {
            participants.push(message['userId'])
        }
    })

    return participants.length;
}

export const getLastMessageDate = (messages: IMessage[]): Date => {
    return messages[messages.length - 1]['createdAt'];
}
