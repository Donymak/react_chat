import {configureStore} from '@reduxjs/toolkit'
import * as services from '../services/services'
import {authReducer} from './root_reducer'

export const store = configureStore({
    reducer: {
        authReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        thunk: {
            extraArgument: {services},
        },
    }),
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch