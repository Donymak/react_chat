import React from "react";
import {Container, Navbar, NavbarBrand} from "react-bootstrap";
import NavbarCollapse from "react-bootstrap/NavbarCollapse";

import "./header.css"

interface IHeaderProps {
    chatName: string
    participantsCount: number
    messagesCount: number
    lastMessageDate: string
}

const Header: React.FC<IHeaderProps> = ({chatName, participantsCount, messagesCount, lastMessageDate}) => {
    return (
        <Navbar className="mt-5 bg-light header">
            <Container className="align-items-end">
                <NavbarBrand href="#" className="header-title mr-5">{chatName}</NavbarBrand>
                <NavbarCollapse className="justify-content-start">
                    <Navbar.Text className="mr-5 header-users-count">
                        {participantsCount} participants
                    </Navbar.Text>
                    <Navbar.Text className="header-messages-count">
                        {messagesCount} messages
                    </Navbar.Text>
                </NavbarCollapse>
                    <Navbar.Text className="header-last-massage-date justify-content-end">
                        Last message: {lastMessageDate}
                    </Navbar.Text>
            </Container>
        </Navbar>
    )
}

export default Header;