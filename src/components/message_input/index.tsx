import React, { useState } from 'react'
import { Button, Form } from 'react-bootstrap'

import './messageInput.css'

interface IMessageInputProps {
    onAddMessage: (message: string) => void
    onEditMessage: (message: string) => void
    text: string
    isEditing: boolean
}

const MessageInput: React.FC<IMessageInputProps> = ({
                                                        onAddMessage, onEditMessage, text, isEditing,
                                                    }) => {
    const [localText, setLocalText] = useState<string>('')
    const [touched, setTouched] = useState<boolean>(false)
    const [isWarning, setIsWarning] = useState<boolean>(false)

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        if (!touched) {
            setTouched(true)
        }

        setLocalText(e.target.value)
    }

    const handleSubmit = (): void => {
        if (localText.trim() === '') {
            setIsWarning(true)
        }

        if (isEditing) {
            onEditMessage(text)
        } else {
            onAddMessage(text)
        }

        setLocalText('')
        setTouched(false)
        setIsWarning(false)
    }

    return (
        <div className="message-input">
            <Form.Control
                value={isEditing && !touched ? text : localText}
                onChange={handleChange}
                className={`message-input-text ${isWarning ? 'warning' : ''}`}
                type="text"
                placeholder={isWarning ? 'Cannot be empty' : 'Message'}
                size="sm"
            />
            <Button
                onClick={handleSubmit}
                type="submit"
                className="message-input-button"
                size="sm"
            >
                {isEditing ? 'Edit' : 'Send'}
            </Button>
        </div>
    )
}

export default MessageInput